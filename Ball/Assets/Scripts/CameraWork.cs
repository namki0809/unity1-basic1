﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWork : MonoBehaviour {
	GameObject ball;

	// Use this for initialization
	void Start () {
		ball = GameObject.Find("Ball"); //지금 스크립트가 속해있는 게임오브젝트가 아닌, 다른 게임오브젝트를 찾아서 불러옴.
		
	}
	
	// Update is called once per frame
	void Update () {

		Debug.Log("I am Camera. And ball is at " + ball.transform.position.z);
		transform.position = new Vector3(0
		,ball.transform.position.y + 3
		,ball.transform.position.z - 14);
		
	}
}
