﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	int count = 1;
	float startingPoint; //시작 지점 좌표를 불러올 변수 선언.

	//  SphereCollider myCollider; //SphereCollider를 맴버 변수 myCollider로 선언.

	bool shouldPrintOver20 = true;
	bool shouldPrintOver30 = true;

	// Use this for initialization
	void Start () {
		
		myCollider = GetComponent<SphereCollider>();
		Rigidbody myRigidbody=GetComponent<Rigidbody>(); //Rigidbody를 지역변수 myRigidbody로 선언후, 컴퍼넌트인 리지드바디의 값을 읽어옴.
		Debug.Log("UseGravity?:" + myRigidbody.useGravity);

		Debug.Log("Start");
		startingPoint = transform.position.z; // 시작과 동시에 지점 좌표를 변수 값으로 지정. 처음 시작 지점 위치 값.

		/*Ball스크립트가 포함된 GameObject 안에서의 inspector를 읽어오거나 조작할 수 있다. 
		이번 것은 Transform 탭에서 Position을 조회한 것.*/
		
	}
	
	// Update is called once per frame
	void Update () {
		//   myCollider.radius= myCollider.radius + 0.1f; // 변수 myCollider로 지정된, 컴포넌트인 SphereCollider의 radius를 조작.
		float distance; //이동 거리를 계산할 변수 선언.
     	distance = transform.position.z - startingPoint; // 업데이트 되는 현재위치에서 시작지점위치를 계속 빼나가면, 이동거리가 나옴.
        
		if (distance > 30)
        {
			if(shouldPrintOver30)
			{
				Debug.Log("Over 30:" + distance);
				shouldPrintOver30 = false;

			}
             
		}
		else if(distance > 20)
		{
			if(shouldPrintOver20)
			{
				Debug.Log("Over 20:" + distance);
				shouldPrintOver20 = false;

			}
			
		}
        

    }






	
}
