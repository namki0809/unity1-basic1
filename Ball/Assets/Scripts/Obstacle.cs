﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	float delta = -0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		float newPosition = transform.position.x + delta; //transform.position.x은 해당 오브젝트의 현재 x좌표를 불러옴.
		transform.position = new Vector3(newPosition,2,-7); //new Vector3(x,y,z)는 해당 게임 오브젝트의 좌표 변경. 
		if(transform.position.x < -3.5)
		{
			delta = 0.1f;
		}		
		else if(transform.position.x > 3.5)
		{
			delta = -0.1f;
		}
	}
}
